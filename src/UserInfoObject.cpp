/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file UserInfoObject.cpp contains the function definitions of the
 *  UserInfoObject class. See UserInfoObject.h for more info.
 */

#include "defines.h"

using namespace std;

#include "UserInfoObject.h"

UserInfoObject::UserInfoObject ( ) {
  //! Algorithm:
  //! <ol>

  //! <li> Initialize variables via InitData().
  InitData ();
  //! </ol>
}

UserInfoObject::UserInfoObject ( const UserInfoObject& rhs ) {
  //! Algorithm:
  //! <ol>

  //! <li> Initialize variables via InitData().
  InitData ();

  //! <li> Copy via overloaded operator=.
  *this = rhs;
  //! </ol>
}

UserInfoObject::~UserInfoObject ( ) {
  // Empty
}

void UserInfoObject::InitData ( ) {
  //! Algorithm:
  //! <ol>

  //! <li> Initialize variables
  _name = L"";
  _state = UserDisconnected;
  _screenLocked = false;
  _recordStale = true;
  _connectTime = 0;
  _loginTime = 0;
  //! </ol>
}
  
wstring UserInfoObject::GetName ( ) {
  return _name;
}

void UserInfoObject::SetName ( wstring name ) {
  _name = name;
}

USER_INFO_CONNECTION_STATE UserInfoObject::GetConnectionState ( ) {
  return _state;
}

void UserInfoObject::SetConnectionState ( USER_INFO_CONNECTION_STATE newState 
					  ) {
  _state = newState;
};

bool UserInfoObject::IsScreenLocked ( ) {
  return _screenLocked;
}

void UserInfoObject::SetScreenLocked ( bool locked ) {
  _screenLocked = locked;
}

bool UserInfoObject::GetRecordStale ( ) {
  return _recordStale;
}
  
void UserInfoObject::SetRecordStale ( bool isStale ) {
  _recordStale = isStale;
}


UserInfoObject& UserInfoObject::operator= ( const UserInfoObject& rhs ) {
  //! Algorithm:
  //! <ol>

  //! <li> Check for self-assignment.
  if ( this == &rhs ) {
    return *this;
  }

  //! <li> Copy over internal data structures.
  _name = rhs._name;
  _state = rhs._state;
  _screenLocked = rhs._screenLocked;
  _recordStale = rhs._recordStale;
  _connectTime = rhs._connectTime;
  _loginTime = rhs._loginTime;

  //! <li> Return
  return *this;
  //! </ol>
}

void UserInfoObject::SetConnectTime ( const time_t& connectTime ) {
  _connectTime = connectTime;
}

time_t UserInfoObject::GetConnectTime ( ) {
  return _connectTime;
}

void UserInfoObject::SetLoginTime ( const time_t& loginTime ) {
  _loginTime = loginTime;
}

time_t UserInfoObject::GetLoginTime ( ) {
  return _loginTime;
}

bool UserInfoObject::operator== ( const UserInfoObject& rhs ) {
  if( _name != rhs._name ) {
    return false;
  }
  if( _state != rhs._state ) {
    return false;
  }
  if( _screenLocked != rhs._screenLocked ) {
    return false;
  }
  if( _recordStale != rhs._recordStale ) {
    return false;
  }
  if( _connectTime != rhs._connectTime ) {
    return false;
  }
  if( _loginTime != rhs._loginTime ) {
    return false;
  }
  return true;
}

bool UserInfoObject::operator!= ( const UserInfoObject& rhs ) {
  return !( *this == rhs );
}
