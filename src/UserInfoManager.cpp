/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file UserInfoManager.cpp contains the function definitions of the
 *  UserInfoManager class. See UserInfoManager.h for more info.
 */

#include "defines.h"

#include <windows.h>
#include <wtsapi32.h>

#include <sstream>

using namespace std;

#include "UserInfoManager.h"

UserInfoManager::UserInfoManager () {
  // Empty
}

UserInfoManager::~UserInfoManager () {
  // Empty
}

void UserInfoManager::Initialize ( ServiceLogger logger, 
				   ConfigManager configManager ) {
  // Copy into local structures
  _logger = logger;
  _configMgr = configManager;
}
  
void UserInfoManager::UpdateUserStatus ( DWORD eventType, 
					 DWORD sessionID ) {

  // Used in debug mode.
  wostringstream tmpMsg;
  // Pointer for the session info buffer that we'll request via
  // WTSEnumerateSessions(). Should be freed via WTSFreeMemory().
  PWTS_SESSION_INFO ppSessionInfo = NULL;
  // Number of sessions returned by WTSEnumerateSessions
  DWORD numSessions;
  // Used to hold a pointer to the buffer that is returned by
  // WTSQuerySessionInformation(). Should be freed via WTSFreeMemory().
  LPTSTR ppBuff = NULL;
  // Will hold the size of the result from WTSQuerySessionInformation().
  DWORD buffSize = 0;
  // Iterator used to iterate over user info database.
  map<wstring, UserInfoObject>::iterator itr;
  // Temporary UserInfoObject
  UserInfoObject* pTmpUsrInfo = NULL;
  // Local copy of the session ID, as specified in the session record.
  DWORD lclSessionID;
  // Holds the user name associated with a session as we iterate through the
  // login sessions.
  wstring userName;

  //! Algorithm:
  //! <ol>
  
  //! <li> If debug mode is enabled, log the event type.
  if ( _configMgr.InDebugMode () ) {
    tmpMsg << L"[UserInfoManager:;UpdateUserStatus] Session ID = " 
	   << sessionID << ", Event = ";
    
    switch ( eventType ) {
    case WTS_CONSOLE_CONNECT:
      tmpMsg << L"WTS_CONSOLE_CONNECT";
      break;
    case WTS_CONSOLE_DISCONNECT:
      tmpMsg << L"WTS_CONSOLE_DISCONNECT";
      break;
    case WTS_REMOTE_CONNECT:
      tmpMsg << L"WTS_REMOTE_CONNECT";
      break;
    case WTS_REMOTE_DISCONNECT:
      tmpMsg << L"WTS_REMOTE_DISCONNECT";
      break;
    case WTS_SESSION_LOGON:
      tmpMsg << L"WTS_SESSION_LOGON";
      break;
    case WTS_SESSION_LOGOFF:
      tmpMsg << L"WTS_SESSION_LOGOFF";
      break;
    case WTS_SESSION_LOCK:
      tmpMsg << L"WTS_SESSION_LOCK";
      break;
    case WTS_SESSION_UNLOCK:
      tmpMsg << L"WTS_SESSION_UNLOCK";
      break;
    case WTS_SESSION_REMOTE_CONTROL:
      tmpMsg << L"WTS_SESSION_REMOTE_CONTROL";
      break;
      /*
	case WTS_SESSION_CREATE:
	tmpMsg << L"WTS_SESSION_CREATE";
	break;
	case WTS_SESSION_TERMINATE:
	tmpMsg << L"WTS_SESSION_TERMINATE";
	break;
      */
    default:
      tmpMsg << L"Unknown";
      break;
    }
    tmpMsg << L". ";

    // Log event
    _logger.ReportInformation ( tmpMsg.str ().c_str () );

    // Clear any bits set for tmpMsg
    tmpMsg.clear ();
    // Set tmpMsg to an empty string.
    tmpMsg.str ( wstring ( L"" ) );
  }
  
  //! <li> Try to enumerate the current sessions, checking return code of
  //! WTSEnumerateSessions for error. If error:
  //! <ol>
  if ( WTSEnumerateSessions ( 
			     // Specify the local machine.
			     WTS_CURRENT_SERVER_HANDLE, 
			     // Reserved word
			     0,
			     // Version
			     1,
			     // Place to put the data
			     &ppSessionInfo, 
			     // Number of sessions
			     &numSessions ) == 0 ) {

    //! Log error
    _logger.ReportWinError ( L"WTSEnumerateSessions" );
    
    //! Free any memory allocated by WTSEnumerateSessions, if non-NULL.
    if ( ppSessionInfo != NULL ) {
      WTSFreeMemory ( ppSessionInfo );
      ppSessionInfo = NULL;
      numSessions = 0;
    }
  }
  //! </ol>

  //! <li> If debug mode, then log the number of active sessions.
  if ( _configMgr.InDebugMode () ) {
    tmpMsg << L"WTSENumerateSessions returned " << numSessions << " sessions.";

    // Log event
    _logger.ReportInformation ( tmpMsg.str ().c_str () );

    // Clear any bits set for tmpMsg
    tmpMsg.clear ();
    // Set tmpMsg to an empty string.
    tmpMsg.str ( wstring ( L"" ) );
  }
  
  //! <li> Iterate over contents of user database marking each account as stale.
  for ( itr = _users.begin() ; itr != _users.end() ; ++itr ) {
    itr->second.SetRecordStale(true);
  }

  //! <li> Iterate over the list of sessions returned by WTSEnumerateSessions.
  //! For each session:
  //! <ol>
  if ( 0 < numSessions ) {
    for ( DWORD i = 0 ; i < numSessions ; i++ ) {
      //! <li> Grab a local copy of the session ID from the session record.
      lclSessionID = ppSessionInfo[i].SessionId;

      //! <li> If debug enabled, stuff the session info into a temporary buffer.
      if ( _configMgr.InDebugMode () ) {
	tmpMsg << "Index: " << i << ",";
	tmpMsg << "Session ID: " << lclSessionID << ", ";
	tmpMsg << "Station Name: " << ppSessionInfo[i].pWinStationName << ", ";
	tmpMsg << "Connect State: ";
	switch ( ppSessionInfo[i].State ) {
	case WTSActive: 
	  tmpMsg << "WTSActive";
	  break;
	case WTSConnected:
	  tmpMsg << "WTSConnected";
	  break;
	case WTSConnectQuery:
	  tmpMsg << "WTSConnectQuery";
	  break;
	case WTSShadow:
	  tmpMsg << "WTSShadow";
	  break;
	case WTSDisconnected:
	  tmpMsg << "WTSDisconnected";
	  break;
	case WTSIdle:
	  tmpMsg << "WTSIdle";
	  break;
	case WTSListen:
	  tmpMsg << "WTSListen";
	  break;
	case WTSReset:
	  tmpMsg << "WTSReset";
	  break;
	case WTSDown:
	  tmpMsg << "WTSDown";
	  break;
	case WTSInit:
	  tmpMsg << "WTSInit";
	  break;
	default:
	  tmpMsg << "Unknown state!";
	  break;
	}
      }

      //http://www.codeproject.com/Articles/18179/Using-the-Local-Security-Authority-to-Enumerate-Us
      //! <li> Attempt to determine user name from session ID via
      //! WTSQuerySessionInformation(), checking return code for error. 
      //! If error:
      //! <ol>
      if ( WTSQuerySessionInformation (
				       // Specify the local machine.
				       WTS_CURRENT_SERVER_HANDLE,
				       // Sepcify the session ID passed in.
				       //sessionID,
				       lclSessionID,
				       // Indicate that we want the user name
				       WTSUserName,
				       // Pointer to a pointer that will point 
				       // to the result (yay system calls!)
				       &ppBuff,
				       // Size of result
				       &buffSize ) == 0 ) {

	//! <li> Report the error
	_logger.ReportWinError ( L"WTSQuerySessionInformation" );

	//! <li> If in debug mode, append a note to the temporary message
	//! regarding the error, log message, and clear message out.
	if ( _configMgr.InDebugMode () ) {
	  tmpMsg << " >> Attempt to query session information resulted in "
		 << "an error!";
	  _logger.ReportInformation ( tmpMsg.str ().c_str () );

	  // Clear any bits set for tmpMsg
	  tmpMsg.clear ();
	  // Set tmpMsg to an empty string.
	  tmpMsg.str ( wstring ( L"" ) );
	}
	
	//! <li> Free the session query buffer, if allocated.
	if ( ppBuff != NULL ) {
	  WTSFreeMemory ( ppBuff );
	  ppBuff = NULL;
	  buffSize = 0;
	}
      }
      //! </ol>

      //! <li> Convert the resulting LPTSTR to wstring.
      userName = ppBuff;

      //! <li> Update user record based on current state:
      //! <ul>
      switch ( ppSessionInfo[i].State ) {
      case WTSActive: 
	//! <li> WTSActive means that the user is logged into the console. Find
	//! record and mark accordingly, adding if user is now logging on.
	pTmpUsrInfo = GetUserRecord ( ppBuff );
	if ( pTmpUsrInfo != NULL ) {
	  pTmpUsrInfo->SetRecordStale ( false );
	  if( eventType == WTS_SESSION_LOCK ) {
	    pTmpUsrInfo->SetScreenLocked( true );
	  }
	  if( eventType == WTS_SESSION_UNLOCK ) {
	    pTmpUsrInfo->SetScreenLocked( false );
	  }
	  if( pTmpUsrInfo->GetConnectionState( ) == UserDisconnected ) {
	    pTmpUsrInfo->SetConnectTime( time( 0 ) );
	  }
	  pTmpUsrInfo->SetConnectionState ( UserConnectedConsole ) ;
	}
	break;
      case WTSConnected:
	//! <li> WTSConnected means that the user is logged in remotely. Find
	//! record and mark accordingly, adding if user is now logging on.
	pTmpUsrInfo = GetUserRecord ( ppBuff );
	if ( pTmpUsrInfo != NULL ) {
	  pTmpUsrInfo->SetRecordStale ( false );
	  pTmpUsrInfo->SetScreenLocked ( false );
	  if( pTmpUsrInfo->GetConnectionState( ) == UserDisconnected ) {
	    pTmpUsrInfo->SetConnectTime( time( 0 ) );
	  }
	  pTmpUsrInfo->SetConnectionState ( UserConnectedRemote ) ;
	}

	break;
      case WTSDisconnected:
	//! <li> WTSDisconnected means that the user is still logged in but
	//! not connected.  Find record and mark accordingly, adding if user is
	//! now logging on.
	pTmpUsrInfo = GetUserRecord ( ppBuff );
	if ( pTmpUsrInfo != NULL ) {
	  pTmpUsrInfo->SetRecordStale ( false );
	  pTmpUsrInfo->SetScreenLocked ( true );
	  if( pTmpUsrInfo->GetConnectionState( ) == UserConnectedConsole ||
	      pTmpUsrInfo->GetConnectionState( ) == UserConnectedRemote ) {
	    pTmpUsrInfo->SetConnectTime( time( 0 ) );
	  }
	  pTmpUsrInfo->SetConnectionState ( UserDisconnected ) ;
	}
	break;
      case WTSConnectQuery:
      case WTSShadow:
      case WTSIdle:
      case WTSListen:
      case WTSReset:
      case WTSDown:
      case WTSInit:
      default:
	//! <li> Otherwise, do nothing.
	break;
      }
      //! </ul>

      //! <li> If in debug mode append user name to temporary message and log 
      //! it.
      if ( _configMgr.InDebugMode () ) {
	tmpMsg << L" user name: '" << userName << L"': " << (long)pTmpUsrInfo;
	_logger.ReportInformation ( tmpMsg.str ().c_str () );
	
	// Clear any bits set for tmpMsg
	tmpMsg.clear ();
	// Set tmpMsg to an empty string.
	tmpMsg.str ( wstring ( L"" ) );
      }

      //! <li> Free Query Session Information buffer memory
      if ( ppBuff != NULL ) {
	WTSFreeMemory ( ppBuff );
	ppBuff = NULL;
	buffSize = 0;
      }
    }
  }

  //! <li> Free Enumerate Sessions buffer memory
  if ( ppSessionInfo != NULL ) {
    WTSFreeMemory ( ppSessionInfo );
    ppSessionInfo = NULL;
    numSessions = 0;
  }

  //! <li> Iterate over contents of user database deleting all stale records.
  itr = _users.begin();
  while ( itr != _users.end() ) {
    if ( itr->second.GetRecordStale () ) {
      // If record is stale, grab a pointer to it.
      pTmpUsrInfo = &(itr->second);
      // Increment iterator;
      ++itr;
      // Delete record.
      _users.erase ( pTmpUsrInfo->GetName () );
      pTmpUsrInfo = NULL;
    } else {
      // If record isn't stale, simply increment iterator.
      ++itr;
    }
  }

  // begin tmp  
  tmpMsg.clear ();
  tmpMsg.str ( wstring ( L"" ) );
  tmpMsg << L"[[[ STATUS ]]] " << _users.size() << L" elements: ";
  for ( itr = _users.begin() ; itr != _users.end() ; itr++ ) {
    tmpMsg << L"User: " << itr->second.GetName() << L", ";
    switch( itr->second.GetConnectionState() ) {
    case 1:
      tmpMsg << L"Disconnected, ";
      break;
    case 2:
      tmpMsg << L"Disconnected, ";
      break;
    case 3:
      tmpMsg << L"Console, ";
      break;
    case 4:
      tmpMsg << L"Remote, ";
      break;
    default:
      tmpMsg << L"UNKNOWN, ";
      break;
    }
    tmpMsg << (itr->second.IsScreenLocked() ? L"Locked, " : L"Unlocked, ");
    struct tm* timeinfo;
    time_t rawtime = itr->second.GetLoginTime();
    timeinfo = localtime( &rawtime );
    tmpMsg << L"Login: " << asctime(timeinfo) << ", ";
    rawtime = itr->second.GetConnectTime();
    timeinfo = localtime( &rawtime );
    tmpMsg << L"Connect: " << asctime(timeinfo) << ". ";
  }
  _logger.ReportInformation ( tmpMsg.str ().c_str () );
  // end tmp
  //! </ol>
}

UserInfoObject UserInfoManager::GetUserInfo ( wstring userName ) {

  map<wstring, UserInfoObject>::iterator itr;
  UserInfoObject retVal;

  //! Algorithm:
  //! <ol>

  //! <li> Attempt to locate the specified user in the database.
  itr = _users.find ( userName );
  
  //! <li> Make sure we found an actual user. If so, copy user info
  //! into a new UserInfoObject to be returned.
  if ( itr != _users.end() ) {
    retVal = itr->second;
  }

  //! <li> Return either the specified user or an empty user object.
  return retVal;
  //! </ol>
}

vector<UserInfoObject> UserInfoManager::GetLoggedInUsers ( ) {

  vector<UserInfoObject> retVal;
  map<wstring, UserInfoObject>::iterator itr;
  unsigned int numUserInfoObjects;

  //! Algorithm:
  //! <ol>

  //! <li> Check the size of the vector to be returned. If it is smaller than
  //! the number of entries in the user info database, expand it so that it
  //! is large enough.
  numUserInfoObjects = _users.size ();
  if ( retVal.size () < numUserInfoObjects ) {
    retVal.resize ( numUserInfoObjects );
  }

  //! <li> Iterate over contents of user database, creating a vector of
  //! values.
  for ( itr = _users.begin() ; itr != _users.end() ; ++itr ) {
    retVal.push_back ( itr->second );
  }

  return retVal;
  //! </ol>
}

UserInfoObject* UserInfoManager::GetUserRecord ( wstring userName ) {

  UserInfoObject* pRetVal = NULL;
  map<wstring, UserInfoObject>::iterator itr;

  //! Algorithm:
  //! <ol>

  //! <li> Attempt to locate the specified user in the database.
  itr = _users.find ( userName );
// Used in debug mode.
wostringstream tmpMsg;
  
  //! <li> Make sure we found an actual user. If not, create a new
  //! record and insert it into the database. Otherwise, return
  //! the found record.
  if ( itr == _users.end() ) {
    // User isn't in database. Create a new record in the database.
    pRetVal = &_users [ userName ];

    // If a new record was successfully created, set the name and connection
    // time of the new record.
    if ( pRetVal != NULL ) {
      pRetVal->SetName ( userName );
      pRetVal->SetLoginTime ( time ( 0 ) );
    }
tmpMsg << L"Created new record for user: " << userName;    
_logger.ReportInformation ( tmpMsg.str ().c_str () );    
  } else {
    pRetVal = &itr->second;
tmpMsg << L"Found record for user: " << userName;    
_logger.ReportInformation ( tmpMsg.str ().c_str () );
  }

  //! <li> Return reference to user record.
  return pRetVal;

  //! </ol>
}
