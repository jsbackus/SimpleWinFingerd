/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file ServiceLogger.h contains the definition of the ServiceLogger class.
 */

#ifndef __SERVICELOGGER_H__
#define __SERVICELOGGER_H__

#include "defines.h"

#include <windows.h>

#include "WinException.h"

/*! \brief ServiceLogger is a class that is responsible for logging events with 
 *  via Windows ReportEvent().
 * 
 *  ServiceLogger is essentially a convenience wrapper around ReportEvent(), 
 *  where each instance is related to a specific service.
 */
class ServiceLogger {

 public:
  //! Maximum allowed length of the service name.
  static const int MAX_SERVICE_NAME_LENGTH = 80;

  /*! \brief Default constructor.
   */
  ServiceLogger () throw();

  /*! \brief Constructor that allows the caller to specify the name of the 
   *  service.
   */
  ServiceLogger ( const wchar_t* serviceName //!< [in] Name of the service.
		  ) throw();
  
  /*! \brief Constructor that will duplicate another ServiceLogger.
   */
  ServiceLogger ( ServiceLogger& rhs //!< [in] ServiceLogger to duplicate.
		  ) throw();
  
  ~ServiceLogger () throw();

  /*! \brief Overloaded Operator=.
   */
  ServiceLogger& operator= ( const ServiceLogger &rhs //!< [in] Right-hand side 
			     //!< of statement.
			     ) throw();

  /*! \brief Sets the service name related to this ServiceLogger object.
   * 
   *  Copies the name pointed to by serviceName into an internal buffer and then
   *  retrieves a handle to the event log for this service. 
   *
   *  Note: Only event logs on the local computer are supported.
   */
  void SetServiceName ( const wchar_t* serviceName ) throw();

  /*! \brief Report an informational event.
   */
  void ReportInformation ( const wchar_t* eventMessage //!< [in] Message to report.
			   ) throw();

  /*! \brief Report a warning event.
   */
  void ReportWarning ( const wchar_t* eventMessage //!< [in] Message to report.
		       ) throw();

  /*! \brief Report an error event.
   */
  void ReportError ( const wchar_t* errorMessage, //!< [in] Message to report.
		     WORD errorCode //!< [in] Error code to use.
		     ) throw();

  /*! \brief Report an error in a Windows function.
   *
   *  Calls GetLastError() and includes it in the message and uses lower word as
   *  the error code.
   */
  void ReportWinError ( const wchar_t* functionName //!< [in] Function that failed.
			) throw();

  /*! \brief Report a success event.
   */
  void ReportSuccess ( const wchar_t* eventMessage //!< [in] Message to report.
		       ) throw();

  /*! \brief Logs a WinException to the event log.
   */
  void ReportException ( WinException e //!< [in] WinException to log.
			 ) throw();

  /*! \brief Report a general event.
   *
   *  See [ReportEvent][http://msdn.microsoft.com/en-us/library/windows/desktop/aa363679%28v=vs.85%29.aspx]
   *  for more information on event types.
   */
  void LogEvent ( const wchar_t* eventMessage, //!< [in] Message to report.
		  WORD eventType, //!< [in] Event type, as defined for WINAPI 
		  //!< functionReportEvent().
		  WORD eventCode //!< [in] Event code. Should be error code or 0
		  //!< for no error.
		  ) throw();
 private:
  /*! \brief Deregisters _eventSource and deletes _serviceName.
   */
  void DeleteServiceName () throw();

  //! Maintains the name of the service. Note that the maximum length of this 
  //! string is MAX_SERVICE_NAME_LENGTH plus room for the terminating NULL.
  wchar_t _serviceName [ MAX_SERVICE_NAME_LENGTH + 1 ];

  //! Maintains a handle to the specified event log.
  HANDLE _eventSource;
};

#endif
