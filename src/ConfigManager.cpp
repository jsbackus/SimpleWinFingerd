/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file ConfigManager.h contains the definition of the ConfigManager class.
 */

#include "defines.h"

#include <sstream>

#include "ConfigManager.h"
#include "WinException.h"

ConfigManager::ConfigManager () {
  InitData();
}

ConfigManager::ConfigManager ( wstring serviceName ) {
  InitData();

  SetServiceName ( serviceName );
}
  
ConfigManager::ConfigManager ( ConfigManager& rhs ) {
  InitData();

  SetServiceName ( rhs._serviceName );
}
  
ConfigManager::~ConfigManager () {
  // Close the registry key, if necessary
  if ( _registryKey != NULL) {
    RegCloseKey ( _registryKey );
    _registryKey = NULL;
  }
}

ConfigManager& ConfigManager::operator= ( const ConfigManager &rhs ) {
  //! Algorithm:
  //! <ol>

  //! <li> Check for self-assignment. If same object, then simply return.
  if ( this == &rhs ) {
    return *this;
  }

  //! <li> Call SetServiceName() with the name of rhs.
  SetServiceName ( rhs._serviceName );

  //! <li> Copy over logger object.
  _logger = rhs._logger;

  //! <li> Copy over copies of registry fields.
  _debugMode = rhs._debugMode;

  //! <li> Copy local values valid flag.
  _localValuesValid = rhs._localValuesValid;
  
  //! <li> Return
  return *this;
  //! </ol>
}

void ConfigManager::SetLogger ( const ServiceLogger& logger ) {
  _logger = logger;
}

  
void ConfigManager::SetServiceName ( wstring serviceName ) {
  HKEY tmpKey = NULL;

  //! Algorithm: 
  //! <ol>

  //! <li> Close the registry key, if necessary
  if ( _registryKey != NULL) {
    RegCloseKey ( _registryKey );
    _registryKey = NULL;
  }

  //! <li> Change service name
  _serviceName = serviceName;

  //! <li> Open the registry key for the service, if the name is not "". All 
  //! config options will be subkeys of this key. Use RegCreateKeyEx to create
  //! the any missing superkeys.
  //! <ol>

  if ( !_serviceName.empty () ) {
    //! <li> First, open HKEY_CURRENT_USER\Software
    RegCreateKeyEx ( 
		    // Base key
		    HKEY_CURRENT_USER, 
		    // Subkey
		    L"Software", 
		    // Reserved word, must be 0.
		    0, 
		    // Class of key
		    NULL, 
		    // Registry key options
		    REG_OPTION_NON_VOLATILE, 
		    // Security Access Mode.
		    KEY_CREATE_SUB_KEY | KEY_QUERY_VALUE | KEY_READ | 
		    KEY_SET_VALUE | KEY_WRITE, 
		    // Secuirty Attributes - NULL means it cannot be inherited.
		    NULL, 
		    // Destination HKEY
		    &tmpKey, 
		    // Place to stick disposition value.
		    NULL );
    
    //! <li> Validate return value.
    if ( tmpKey == NULL ) {
      throw 
	WinException ( 1, 
		       L"Cannot create or access HKEY_CURRENT_USER\\Software!" );
    }
    DWORD dispo = 0;  
    //! <li> Next, open HKEY_CURRENT_USER\Software\<SERVICE_NAME>
    RegCreateKeyEx ( 
		    // Base key
		    tmpKey, 
		    // Subkey
		    _serviceName.c_str (), 
		    // Reserved word, must be 0.
		    0, 
		    // Class of key
		    NULL, 
		    // Registry key options
		    REG_OPTION_NON_VOLATILE, 
		    // Security Access Mode. Going with default, which should be
		    // parent's SAM.
		    KEY_CREATE_SUB_KEY | KEY_QUERY_VALUE | KEY_READ | 
		    KEY_SET_VALUE | KEY_WRITE, 
		    // Secuirty Attributes - NULL means it cannot be inherited.
		    NULL, 
		    // Destination HKEY
		    &_registryKey, 
		    // Place to stick disposition value.
		    //NULL );
		    &dispo );
    //! <li> Close temporary key and validate output.
    RegCloseKey ( tmpKey );
    tmpKey = NULL;

    if ( _registryKey == NULL ) {
      throw 
	WinException ( 1,
		       L"Cannot create or access registry entry for service!" );
    }
    //! </ol>

    //! Probe registry for values held internally, if local values are not
    //! valid.
    if ( !_localValuesValid ) {
      ProbeRegistry ();
    }

  }
}

wstring ConfigManager::GetString ( wstring key ) {

  const DWORD BUFF_SIZE = 2048;

  // Create a buffer of 2048 bytes (max recommended size for registry) plus 
  // room for an additional NULL to place initial string value into.
  wchar_t buff[ ( BUFF_SIZE / sizeof ( wchar_t ) ) + sizeof ( wchar_t ) ];

  // Holds the size of the buffer (minus the space for the NULL termination) 
  // for call to RegQueryValueEx, but holds size of actual string on return.
  DWORD valSize;
  
  // We want to retrieve the type of value to verify that it is a string.
  DWORD valType;

  // String to return
  wstring retVal;

  // Will hold the error code from RegQueryValueEx().
  long result = ERROR_MORE_DATA;

// *** TO DO ***
// http://www.cplusplus.com/reference/string/string/resize/

  //! Algorithm:
  //! <ol>

  //! <li> While there is data to retrieve:
  //! <ol>
  while ( result == ERROR_MORE_DATA ) {
    //! <li> Populate valSize with size of the buffer.
    valSize = sizeof ( buff ) - sizeof ( wchar_t );

    //! <li> Attempt to retrieve string and string size.
    result = RegQueryValueEx (
			      // Registry key
			      _registryKey,
			      // Value/"subkey" name
			      key.c_str (),
			      // Reserved word, must be 0
			      0,
			      // Data type
			      &valType,
			      // Buffer for data
			      (LPBYTE) &buff,
			      // size of buffer
			      &valSize );
    
    //! <li> Validate data / check function return for error:
    //! <ul>
    if ( valType != REG_SZ && valType != REG_EXPAND_SZ ) {
      //! <li> If data type is not REG_SZ or REG_EXPAND_SZ, throw an exception.
      wostringstream exbuff;
      exbuff << "Error: registry value '" << key 
	     << "' not of type REG_SZ or REG_EXPAND_SZ!";
      throw exbuff.str().c_str();
    }

    //! <li> If error code is not ERROR_MORE_DATA or ERROR_SUCCESS then throw
    //! an exeception.
    if ( result != ERROR_SUCCESS && result != ERROR_MORE_DATA ) {
      wostringstream exbuff;
      exbuff << "Error: attempt to read registry value '" << key 
	     << "' resulted in error code " << result;
      throw exbuff.str().c_str();
    }

    //! </ul>

    //! <li> Check the size of the data returned:
    //! <ul>
    
    //! <li> If it is larger than the provided buffer, throw an exception.
    if ( BUFF_SIZE < valSize ) {
      wostringstream exbuff;
      exbuff << "Error: attempt to read registry value '" << key 
	     << "' exceeded buffer size by " << ( BUFF_SIZE - valSize) 
	     << " bytes!";
      throw exbuff.str().c_str();
    }

    //! <li> If it is non-zero, then:
    if ( 0 < valSize ) {
      //! <ol>

      //! <li> Verify that the end of the string is NULL-terminated.
      if ( buff [ valSize ] != (wchar_t)NULL ) {
	buff [ valSize + 1 ] = (wchar_t)NULL;
      }
      
      //! <li> Append buffer to end of string to return.
      retVal.append ( buff );
      
      //! </ol>
    }

    //! </ul>
  }
  //! </ol>

  //! <li> Clean up and return.
  return retVal;

  //! </ol>
}

void ConfigManager::SetString ( wstring key, 
				wstring value ) {

  // Grab a pointer to the NULL-terminated data.
  const wchar_t* pVal = value.c_str ();
  
  // Size of string in bytes.
  DWORD valSize = value.size () * sizeof ( wchar_t );

  // Will hold the error code from RegQueryValueEx().
  long result;

  //! Algorithm:
  //! <ol>

  //! <li> Call RegSetValueEx
  result = RegSetValueEx (
			  // Registry key
			  _registryKey,
			  // Value/"subkey" name
			  key.c_str (),
			  // Reserved word, must be 0.
			  0,
			  // Data type
			  REG_SZ,
			  // pointer to data
			  (LPBYTE) pVal,
			  // Size of data
			  valSize );
  
  //! <li> Look at the return value and handle any errors.
  if ( result != ERROR_SUCCESS ) {
    throw "Error setting registry value!";
  }

  //! </ol>
}

DWORD ConfigManager::GetDWord ( wstring key ) {

  // Place to put the data
  DWORD retVal;

  // We want to retrieve the type as well as the value to make sure that it is
  // a DWORD.
  DWORD valType;

  // Initially holds the size of our buffer, but will hold the size of the value
  // after RegQueryValueEx().
  DWORD valSize = sizeof ( retVal );

  // Will hold the error code from RegQueryValueEx().
  long result;
  
  // buffer to generate exception message
  wostringstream exMsg;

  //! Algorithm:
  //! <ol>

  //! <li> Attempt to retrieve value.
  result = RegQueryValueEx (
			    // Registry key
			    _registryKey,
			    // Value/"subkey" name
			    key.c_str (),
			    // Reserved word, must be 0
			    0,
			    // Data type
			    &valType,
			    // Buffer for data
			    (LPBYTE) &retVal,
			    // size of buffer
			    &valSize );

  //! <li> Validate data / check function return for error:
  //! <ul>
  /*
  if ( valType != REG_DWORD && valType != REG_DWORD_LITTLE_ENDIAN ) {
    //! <li> If type isn't a REG_DWORD or REG_DWORD_LITTLE_ENDIAN, throw an
    //! exception.
    exMsg << "Error: registry value '" << key << "' not of type DWORD!";
    throw WinException( 99, exMsg.str().c_str() );
  }
  */
  switch ( result ) {
  case ERROR_SUCCESS:
    //! <li> If ERROR_SUCCESS, then return retrieved value.
    break;
  case ERROR_MORE_DATA:
    //! <li> If ERROR_MORE_DATA, then throw an exception indicating that this is
    //! a DWORD array.
    exMsg << "Error: registry value '" << key << "' is a DWORD array of size " 
	 << ( valSize / sizeof ( DWORD ) );
    throw WinException( result, exMsg.str().c_str());
  default:
    //! <li> Throw a general exception for anything else.
    exMsg << "Error: attempt to read registry value '" << key 
	 << "' resulted in error code " << result;
    throw WinException( result, exMsg.str().c_str());
  }
  //! </ul>

  return retVal;

  //! </ol>
}

void ConfigManager::SetDWord ( wstring key, 
			       const DWORD value ) {

  // Will hold the error code from RegQueryValueEx().
  long result;

  //! Algorithm:
  //! <ol>

  //! <li> Call RegSetValueEx
  result = RegSetValueEx (
			       // Registry key
			       _registryKey,
			       // Value/"subkey" name
			       key.c_str (),
			       // Reserved word, must be 0.
			       0,
			       // Data type
			       REG_DWORD,
			       // pointer to data
			       (LPBYTE) &value,
			       // Size of data
			       sizeof ( value ) );
  
  //! <li> Look at the return value and handle any errors.
  if ( result != ERROR_SUCCESS ) {
    throw "Error setting registry value!";
  }

  //! </ol>
}

void ConfigManager::ProbeRegistry ( ) {
  //! Algorithm:
  //! <ol>

  //! <li> Mark local values dirty in case an issue crops up while loading.
  _localValuesValid = false;

  //! <li> Try to grab debug mode. If there is a problem, log the exception
  //! and enable debugging.
  try {
    _debugMode = (bool) GetDWord( L"enable_debug_mode" );
  } catch ( WinException e ) {
    _logger.ReportException ( e );
    _debugMode = true;
  }

  //! Mark local values clean.
  _localValuesValid = true;
  //! </ol>
}

void ConfigManager::InitData () {
  // Initialize internal data structures
  _serviceName.clear ();
  _registryKey = NULL;
  _localValuesValid = false;
  _debugMode = false;
}

bool ConfigManager::InDebugMode () {
  return _debugMode;
}

void ConfigManager::SetDebugMode ( bool enableDebugMode ) {
  
  //! Algorithm:
  //! <ol>

  //! <li> Set local copy
  _debugMode = enableDebugMode;

  //! <li> Attempt to update the registry.
  SetDWord ( L"enable_debug_mode", (int) _debugMode );
  
  //! </ol>
}
