/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file logger.cpp contains the function definitions of the ServiceLogger class. See logger.h for more info.
 */

#include "defines.h"

#include <windows.h>

#include <sstream>

using namespace std;

#include "ServiceLogger.h"

ServiceLogger::ServiceLogger () throw() {
  //! Algorithm:
  //! <ol>

  //! <li> Initialize variables
  _serviceName[0] = (wchar_t)NULL;
  _eventSource = NULL;

  //! </ol>
}

ServiceLogger::ServiceLogger ( const wchar_t* serviceName ) throw() {
  //! Algorithm:
  //! <ol>

  //! <li> Initialize variables
  _serviceName[0] = (wchar_t)NULL;
  _eventSource = NULL;

  //! <li> Call SetServiceName()
  SetServiceName ( _serviceName );

  //! </ol>
}

ServiceLogger::ServiceLogger ( ServiceLogger& rhs ) throw() {
  //! Algorithm:
  //! <ol>

  //! <li> Initialize variables
  _serviceName[0] = (wchar_t)NULL;
  _eventSource = NULL;

  //! <li> Call the overloaded operator=.
  *this = rhs;

  //! </ol>
}

ServiceLogger::~ServiceLogger () throw() {
  //! Calls DeleteServiceName().
  DeleteServiceName();
}

ServiceLogger& ServiceLogger::operator= ( const ServiceLogger &rhs ) throw() {
  //! Algorithm:
  //! <ol>
  //! <li> Check for self-assignment.
  if ( this == &rhs ) {
    return *this;
  }

  //! <li> Call SetServiceName() with the name of rhs.
  SetServiceName ( &rhs._serviceName[0] );

  //! <li> Return
  return *this;
  //! </ol>
}

void ServiceLogger::SetServiceName ( const wchar_t* serviceName ) throw() {
  //! Algorithm:
  //! <ol>
  
  //! <li> Deregister current event source and clear the current name.
  DeleteServiceName ();

  //! <li> Copy service name
  wcsncpy ( &_serviceName[0], serviceName, MAX_SERVICE_NAME_LENGTH );

  //! <li> Register an event for this service
  // Note: Not error checking.
  _eventSource = RegisterEventSource(
				     // No server name
				     NULL, 
				     // Specify that it is this service
				     &_serviceName[0] );

  //! </ol>
}
 
void ServiceLogger::ReportInformation ( const wchar_t* eventMessage ) throw() {
  //! Call ServiceLogger::LogEvent() with EVENTLOG_INFORMATION_TYPE type.
  LogEvent ( eventMessage, EVENTLOG_INFORMATION_TYPE, 1 );
}


void ServiceLogger::ReportWarning ( const wchar_t* eventMessage ) throw() {
  //! Call ServiceLogger::LogEvent() with EVENTLOG_WARNING_TYPE type.
  LogEvent ( eventMessage, EVENTLOG_WARNING_TYPE, 2 );
}


void ServiceLogger::ReportError ( const wchar_t* errorMessage, WORD errorCode ) throw() {
  //! Call ServiceLogger::LogEvent() with EVENTLOG_ERROR_TYPE type.
  LogEvent ( errorMessage, EVENTLOG_ERROR_TYPE, errorCode );
}

void ServiceLogger::ReportWinError ( const wchar_t* functionName ) throw() {
  
  wostringstream buff;

  //! Algorithm:
  //! <ol>

  //! <li> Get WINAPI error code.
  DWORD winErrorCode = GetLastError ();

  //! <li> Grab lower 16 bits for error message
  WORD msgErrorCode = WORD ( winErrorCode & 0x0000FFFFL );

  try {
    //! <li> Create the message
    buff << functionName << " failed with " << winErrorCode;

    //! <li> LogEvent the event to the application log.
    ReportError ( buff.str ().c_str (), msgErrorCode );
  } catch (...) {
    ReportError ( L"Exception in ReportWinError!", 1);
  }

  //! </ol>
}

void ServiceLogger::ReportSuccess ( const wchar_t* eventMessage ) throw() {
  //! Call ServiceLogger::LogEvent() with EVENTLOG_SUCCESS type.
  LogEvent ( eventMessage, EVENTLOG_SUCCESS, 0 );
}

void ServiceLogger::ReportException ( WinException e ) throw() {

  //! Algorithm:
  //! <ol>
  
  //! <li> Report the error.
  ReportError ( e.GetErrorMessage (), e.GetErrorCode () );

  //! </ol>
}


void ServiceLogger::LogEvent ( const wchar_t* eventMessage,
			       WORD eventType,
			       WORD eventCode ) throw() {

  LPCTSTR stringBuff[2];

  //! Algorithm:
  //! <ol>

  //! <li> Set the event ID to be the event code.
  DWORD eventID = eventCode;

  //! <li> Determine the Facility code and OR into event ID.
  eventID = eventID | 0x00020000L;

  //! <li> Set the event ID severity based on the event type and OR into event 
  //! ID.
  switch ( eventType ) {
  case EVENTLOG_ERROR_TYPE:
    eventID = 0xC0000000L | eventCode;
    break;

  case EVENTLOG_WARNING_TYPE:
    eventID = 0xA0000000L | eventCode;
    break;

  case EVENTLOG_SUCCESS:
    eventID = 0x00000000L | eventCode;
    break;

  case EVENTLOG_INFORMATION_TYPE:
  default:
    eventID = 0x40000000L | eventCode;
    break;
  }
    
  if ( _eventSource != NULL ) {
    //! <li> Stuff the message into an array of strings for ReportEvent().
    stringBuff[0] = &_serviceName[0];
    stringBuff[1] = eventMessage;

    //! <li> Report the event to the application log.
    ReportEvent (
		 // event log handle
		 _eventSource,
		 // event type
		 eventType,
		 // event category
		 0,
		 // event identifier
		 eventID,
		 // no security identifier
		 NULL,
		 // size of stringBuff array                
		 2,
		 // no binary data: size = 0
		 0,
		 // array of strinsg
		 stringBuff,
		 // no binary data
		 NULL );               
  }

  //! </ol>
}

void ServiceLogger::DeleteServiceName () throw() {

  //! Algorithm:
  //! <ol>

  //! <li> Unregister the event source.
  if ( _eventSource != NULL) {
    // Note: not error checking.
    DeregisterEventSource ( _eventSource );
    _eventSource = NULL;
  }

  //! <li> Clear old service name.
  _serviceName[0] = (wchar_t)NULL;

  //! </ol>
}


