/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file ConfigManager.h contains the definition of the ConfigManager class.
 */

#ifndef __CONFIGMANAGER_H__
#define __CONFIGMANAGER_H__

#include "defines.h"

#include <windows.h>
#include <string>

using namespace std;

#include "ServiceLogger.h"

/*! \brief ConfigManager is a convenience interface to the Windows Registry for
 *  SimpleWinFingerd. It is responsible for setting or retrieving various
 *  configuration parameters.
 *
 *  Note: Functions will throw WinException objects on error where appropriate.
 */
class ConfigManager {

 public:
  /*! \brief Default constructor.
   */
  ConfigManager ();

  /*! \brief Constructor that allows the caller to specify the name of the 
   *  service.
   */
  ConfigManager ( wstring serviceName //!< [in] Name of the service.
		  );
  
  /*! \brief Constructor that will duplicate another ConfigManager.
   */
  ConfigManager ( ConfigManager& rhs //!< [in] ConfigManager to duplicate.
		  );
  
  ~ConfigManager ();

  /*! \brief Overloaded Operator=.
   */
  ConfigManager& operator= ( const ConfigManager &rhs //!< [in] Right-hand side
			     //!< of statement.
			     );

  /*! \brief Sets the local logger object.
   */
  void SetLogger ( const ServiceLogger& logger );
  
  /*! \brief Sets the service name related to this ConfigManager object.
   *  Copies the name pointed to by serviceName into an internal buffer. 
   */
  void SetServiceName ( wstring serviceName );

  /*! \brief Retrieves the wstring associated with the specified key.
   *
   *  Notes: 
   *  - Only supports REG_SZ and REG_EXPAND_SZ strings. 
   *  - REG_EXPAND_SZ strings are NOT expanded. Use 
   *  [ExpandEnvironmentStrings](http://msdn.microsoft.com/en-us/library/windows/desktop/ms724265(v=vs.85).aspx)
   */
  wstring GetString ( wstring key //!< [in] Registery key to retrieve.
		     );

  /*! \brief Sets the Registery wstring value associated with the specified key
   *  to the specified new value.
   *
   *  Note: Only supports REG_SZ strings.
   */
  void SetString ( wstring key, //!< [in] Registery key to change the value of.
		   wstring value //!< [in] New Registery value.
		   );

  /*! \brief Gets the integer associated with the specified key.
   */
  DWORD GetDWord ( wstring key //!< [in] Registery key to retrieve.
		   );
  /*! \brief Sets the Registery integer value associated with the specified key
   *  to the specified new value.
   */
  void SetDWord ( wstring key, //!< [in] Registery key to change the value of.
		  const DWORD value //!< [in] New Registery value.
		  );

  /*! \brief Loads local copies of certain settings from the registry.
   */
  void ProbeRegistry ( );

  /*! \brief Returns true if debug is enabled.
   */
  bool InDebugMode ( );

  /*! \brief Enables debug mode.
   *
   * Updates the registery as well.
   */
  void SetDebugMode ( bool enableDebugMode //!< [in] Debug mode is enabled if 
		      //!< true.
		      );

 private:

  /*! \brief Initializes internal data structures.
   */
  void InitData ( );

  //! Flag to indicate that the registry has already been probed. We want
  //! to prevent random behavior caused by the user changing registry values
  //! in between registry reads.
  bool _localValuesValid;

  //! Maintains the name of the service.
  wstring _serviceName;

  //! Local copy of the logger
  ServiceLogger _logger;

  //! Maintains a handle to the specified event log.
  HKEY _registryKey;

  //! Local copy of debug mode.
  bool _debugMode;

};

#endif
