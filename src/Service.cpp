/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file Service.cpp contains the function definitions for the Service class. See Service.h for more
 *  information.
 */

#include "defines.h"

#include <tchar.h>
#include <windows.h>
//#include <winuser.h>
#include <wtsapi32.h>
//#include <ntsecapi.h>

#include <sstream>

using namespace std;

#include "Service.h"
#include "Service_callback.h"


Service::Service () {
  _serviceStatusHandle = 0;
  _serviceStopEvent = NULL;
  _currentState = 0;
  _bSystemShutdown = false;

  _logger.SetServiceName ( SERVICE_NAME );
}
  
Service::~Service () {
  if ( _serviceStopEvent != NULL ) {
    CloseHandle ( _serviceStopEvent );
  }
  _currentState = SERVICE_STOPPED;
  _serviceStatusHandle = 0;
}
  
void Service::ServiceMain ( DWORD argc,
			    LPTSTR* argv) {

  //! - Attempt to register the service
  _serviceStatusHandle = RegisterServiceCtrlHandlerEx ( SERVICE_NAME, 
							&ServiceControlHandler, 
							(void*) this );
  if ( _serviceStatusHandle == 0 ) {
    // Error. Log a message and exit.
    _logger.ReportWinError ( L"RegisterServiceCtrlHandlerEx" );
    return;
  }

  //! - Signal to the SCM "SERVICE_START_PENDING".
  ReportStatus ( SERVICE_START_PENDING );

  //! - Create a "stop event". The control handler, ServiceControlHandler(), will use this to signal a stop.
  _serviceStopEvent = CreateEvent(
				  // Default security attributes
				  NULL,
				  // Require a call to ResetEvent to clear
				  TRUE, 
				  // Initially false
				  FALSE, 
				  // Don't provide a name
				  NULL );
  
  if( _serviceStopEvent == NULL) {
    // We weren't able to create the stop event so abort.
    _logger.ReportError ( L"Unable to create stop event." , 0);
    ReportStatus ( SERVICE_STOPPED );
    return;
  }

  //! <li> Initialize daemon:
  //! <ol>
// *** To Do! ***
  //! <li> Initialize ConfigManager object
  try {
    _configMgr.SetLogger ( _logger );
    _configMgr.SetServiceName ( SERVICE_NAME );

    // begin debug
    /*
    wostringstream tmpMsg;

    // test DWORD
    tmpMsg.clear ();
    _configMgr.SetDWord ( L"SecretOfLife", 42);
    tmpMsg << "Registry Value 'SecretOfLife' = ";
    tmpMsg << _configMgr.GetDWord ( L"SecretOfLife" );
    _logger.ReportInformation ( tmpMsg.str ().c_str () );

    // test string
    tmpMsg.str ().clear (); // <-- This isn't working.
    _configMgr.SetString ( L"Fluffy Bunny", 
			   L"Will Nibble Your Bum" );
    tmpMsg << "Registry Value 'Fluffy Bunny' = '";
    tmpMsg << _configMgr.GetString ( L"Fluffy Bunny" );
    tmpMsg << "'";
    _logger.ReportInformation ( tmpMsg.str ().c_str () );

    // Test exception
    throw WinException ( 99, L"Test Exception");
    */
    // end debug
  } catch ( WinException e ) {
    _logger.ReportException ( e );
  }

  //! <li> Initialize UserInfo object
  try {
    _userMgr.Initialize ( _logger, _configMgr );
  } catch ( WinException e ) {
    _logger.ReportException ( e );
  }

  //! <li> Initialize FingerServer object

  //! </ol>

  //! - Report that the daemon is up and running.
  ReportStatus ( SERVICE_RUNNING );

  //! - Enter main service event loop, waiting for the service to be stopped.
//int i = 0;
  while ( WaitForSingleObject( _serviceStopEvent, 
			       SERVICE_LOOP_SLEEP_MILLISECONDS ) 
	  != WAIT_OBJECT_0 ) {
    // *** To Do! ***
    /*
switch ( i ) {
 case 0:
   _logger.ReportSuccess ( _T ( "Success!" ) );
   break;
 case 1:
   _logger.ReportInformation ( _T ( "N-fomation!" ) );
   break;
 case 2:
   _logger.ReportWarning ( _T ( "Warning?" ) );
   break;
 default:
   _logger.ReportError ( _T ( "Danger Will Robinson!" ), 42 );
   i = -1;
   break;
 }
 i++;
    */
    //! * Update current user record

    //! * Handle TCP socket events

  }

  //! - Report that we're stopping the service
  ReportStatus ( SERVICE_STOP_PENDING );

  //! - Shut down service / release resources. If _bSystemShutdown, we're 
  //! shutting down so no need to release resources, etc.
  if ( !_bSystemShutdown ) {
    // *** To Do! ***
  }

  //! - Destroy stop event
  CloseHandle ( _serviceStopEvent );
  _serviceStopEvent = NULL;

  //! - Report that the service is stopped.
  ReportStatus ( SERVICE_STOPPED );

}

DWORD Service::HandleControlCode ( DWORD controlCode, 
				   DWORD eventType, 
				   void* eventData) {
  //! Algorithm:
  //! <ol>
  DWORD retVal = NO_ERROR;

  //! <li> Examine in-coming control code and react appropriately. Specifically,
  //! if controlCode is:
  //! <ul>
  switch ( controlCode ) {
  case SERVICE_CONTROL_SHUTDOWN:
    //! <li> SERVICE_CONTROL_SHUTDOWN then set flag and continue on to 
    //! SERVICE_CONTROL_STOP.
    _bSystemShutdown = true;
  case SERVICE_CONTROL_STOP:
    //! <li> SERVICE_CONTROL_STOP then report the status change and trigger the
    //! stop event.
    ReportStatus ( SERVICE_STOP_PENDING );
    SetEvent ( _serviceStopEvent );
    break;
  case SERVICE_INTERROGATE:
    //! <li> SERVICE_INTERROGATE then report the current status.
    ReportStatus ( _currentState );
    break;
  case SERVICE_CONTROL_SESSIONCHANGE:
    //! <li> SERVICE_CONTROL_SESSIONCHANGE then handle user status changes.
    UpdateUserStatus ( eventType, *((DWORD*) eventData) );
    break;
  default:
    //! <li> anything else, ignore it and return ERROR_CALL_NOT_IMPLEMENTED
    ReportStatus ( _currentState );
    retVal = ERROR_CALL_NOT_IMPLEMENTED;
    break;
  }
  //! </ul>

  return retVal;

  //! </ol>
}

void Service::ReportStatus ( DWORD state ) {

  //! - Update the global state indicator with the new state.
  _currentState = state;

  //! - Set up the SERVICE_STATUS structure
  SERVICE_STATUS serviceStatus;
  // Only one service in this process
  serviceStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
  // New state
  serviceStatus.dwCurrentState = _currentState;
  // Counter used to indicate progress during start/stop/pause/continue operations. We're holding at
  // zero since we should not have a lengthy start/stop process.
  serviceStatus.dwCheckPoint = 0;
  // An estimate of how much longer until process starts. Not applicable.
  serviceStatus.dwWaitHint = 0;

  // Currently doesn't support error codes. User will need to look at event log.
  serviceStatus.dwWin32ExitCode = NO_ERROR;
  serviceStatus.dwServiceSpecificExitCode = 0;

  // Mask indicating which controls are accepted
  if ( state == SERVICE_START_PENDING ) {
    serviceStatus.dwControlsAccepted = 0;
  } else {
    serviceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP | 
      SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_SESSIONCHANGE;
  }
  
  //! - Give the SCM the current status.
  SetServiceStatus ( _serviceStatusHandle, &serviceStatus );
}

void Service::UpdateUserStatus ( DWORD eventType,
				 DWORD sessionID ) {

#ifdef __USERINFOMANAGER_H__
  _userMgr.UpdateUserStatus ( eventType, sessionID );
#else
  //! Algorithm:
  //! <ol>

  // *** TO DO ***

  //! <li> For now, simply log the event info to the event log
  wostringstream tmpMsg;
  tmpMsg << L"[UpdateStatus] Session ID = " << sessionID << ", Event = ";
  switch ( eventType ) {
  case WTS_CONSOLE_CONNECT:
    tmpMsg << L"WTS_CONSOLE_CONNECT";
    break;
  case WTS_CONSOLE_DISCONNECT:
    tmpMsg << L"WTS_CONSOLE_DISCONNECT";
    break;
  case WTS_REMOTE_CONNECT:
    tmpMsg << L"WTS_REMOTE_CONNECT";
    break;
  case WTS_REMOTE_DISCONNECT:
    tmpMsg << L"WTS_REMOTE_DISCONNECT";
    break;
  case WTS_SESSION_LOGON:
    tmpMsg << L"WTS_SESSION_LOGON";
    break;
  case WTS_SESSION_LOGOFF:
    tmpMsg << L"WTS_SESSION_LOGOFF";
    break;
  case WTS_SESSION_LOCK:
    tmpMsg << L"WTS_SESSION_LOCK";
    break;
  case WTS_SESSION_UNLOCK:
    tmpMsg << L"WTS_SESSION_UNLOCK";
    break;
  case WTS_SESSION_REMOTE_CONTROL:
    tmpMsg << L"WTS_SESSION_REMOTE_CONTROL";
    break;
    /*
  case WTS_SESSION_CREATE:
    tmpMsg << L"WTS_SESSION_CREATE";
    break;
  case WTS_SESSION_TERMINATE:
    tmpMsg << L"WTS_SESSION_TERMINATE";
    break;
    */
  default:
    tmpMsg << L"Unknown";
    break;
  }
  tmpMsg << L". ";

  // Try to enumerate sessions
  PWTS_SESSION_INFO ppSessionInfo = NULL;
  DWORD enumCount;
  // Used to hold a pointer to the buffer that is returned by
  // WTSQuerySessionInformation(). Should be freed via WTSFreeMemory().
  LPTSTR ppBuff = NULL;
  // Will hold the size of the result from WTSQuerySessionInformation().
  DWORD buffSize = 0;

  if ( WTSEnumerateSessions ( 
			     // Specify the local machine.
			     WTS_CURRENT_SERVER_HANDLE, 
			     // Reserved word
			     0,
			     // Version
			     1,
			     // Place to put the data
			     &ppSessionInfo, 
			     // Number of sessions
			     &enumCount ) == 0 ) {
    
    _logger.ReportWinError ( L"WTSQuerySessionInformation" );
    
    if ( ppSessionInfo != NULL ) {
      WTSFreeMemory ( ppSessionInfo );
      ppSessionInfo = NULL;
      enumCount = 0;
    }
  }

  
  if ( 0 < enumCount ) {
    for ( DWORD i = 0 ; i < enumCount ; i++ ) {
      DWORD lclSessionID = ppSessionInfo[i].SessionId;
      /*
      // Clear any bits set for tmpMsg
      tmpMsg.clear ();
      // Set tmpMsg to an empty string.
      tmpMsg.str ( wstring ( L"" ) );
      */

      tmpMsg << "Index: " << i << ",";
      tmpMsg << "Session ID: " << lclSessionID << ", ";
      tmpMsg << "Station Name: " << ppSessionInfo[i].pWinStationName << ", ";
      tmpMsg << "Connect State: ";
      switch ( ppSessionInfo[i].State ) {
      case WTSActive: 
	tmpMsg << "WTSActive";
	break;
      case WTSConnected:
	tmpMsg << "WTSConnected";
	break;
      case WTSConnectQuery:
	tmpMsg << "WTSConnectQuery";
	break;
      case WTSShadow:
	tmpMsg << "WTSShadow";
	break;
      case WTSDisconnected:
	tmpMsg << "WTSDisconnected";
	break;
      case WTSIdle:
	tmpMsg << "WTSIdle";
	break;
      case WTSListen:
	tmpMsg << "WTSListen";
	break;
      case WTSReset:
	tmpMsg << "WTSReset";
	break;
      case WTSDown:
	tmpMsg << "WTSDown";
	break;
      case WTSInit:
	tmpMsg << "WTSInit";
	break;
      default:
	tmpMsg << "Unknown state!";
	break;
      }
      //_logger.ReportInformation ( tmpMsg.str ().c_str () );

      //http://www.codeproject.com/Articles/18179/Using-the-Local-Security-Authority-to-Enumerate-Us
      //! <li> Attempt to determine user name from session ID via
      //! WTSQuerySessionInformation().
      if ( WTSQuerySessionInformation (
				       // Specify the local machine.
				       WTS_CURRENT_SERVER_HANDLE,
				       // Sepcify the session ID passed in.
				       //sessionID,
				       lclSessionID,
				       // Indicate that we want the user name
				       WTSUserName,
				       // Pointer to a pointer that will point to
				       // the result (yay system calls!)
				       &ppBuff,
				       // Size of result
				       &buffSize ) == 0 ) {
	// We've had an error, log error, delete buffer (if necessary) and return.
	// Note: If Remote Desktop Services isn't installed/running, the error code
	// will be 7022.
	_logger.ReportWinError ( L"WTSQuerySessionInformation" );
	
	if ( ppBuff != NULL ) {
	  WTSFreeMemory ( ppBuff );
	  ppBuff = NULL;
	  buffSize = 0;
	}
      }
      /*
      // Clear any bits set for tmpMsg
      tmpMsg.clear ();
      // Set tmpMsg to an empty string.
      tmpMsg.str ( wstring ( L"" ) );
      */
      tmpMsg << ", User Name: " << ppBuff << ".";
      
      //! <li> Free buffer memory
      if ( ppBuff != NULL ) {
	WTSFreeMemory ( ppBuff );
	ppBuff = NULL;
	buffSize = 0;
      }

      //! <li> Log event
      //_logger.ReportInformation ( tmpMsg.str ().c_str () );
      
    }
  
  }
  //! <li> Log event
  _logger.ReportInformation ( tmpMsg.str ().c_str () );

  //! <li> Free buffer memory
  if ( ppSessionInfo != NULL ) {
    WTSFreeMemory ( ppSessionInfo );
    ppSessionInfo = NULL;
    enumCount = 0;
  }

  //! </ol>
#endif
}
