/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file main.cpp contains the main entry point for SimpleWinFingerd.
 */

#include "defines.h"

#include <tchar.h>
#include <windows.h>

#include "Service_callback.h"
#include "ServiceLogger.h"

/*! \brief Main entry point.
 * 
 *  \return Status of success. Valid values:
 *  \return - 0 = Success
 *  \return - -1 = Not successful. See Event Log.
 */
int main(int argc, //!< [in] Number of command-line arguments
	 char **argv //!< [in] Array of individual arguments as char*.
	 ) {

  /*! - Set up a SERVICE_TABLE_ENTRY object and attempt to start SvcMain() via StartServiceCtrlDispatcher().
   */
  SERVICE_TABLE_ENTRY serviceTable[] = { 
    { SERVICE_NAME, // Service name. Ignored when the service is installed with SERVICE_WIN32_OWN_PROCESS.
      &ServiceMain // Pointer to ServiceMain function.
    }, 
    { NULL, NULL } 
  };

  //! - Attempt to start the service
  if ( StartServiceCtrlDispatcher(serviceTable) ) {
    //!   - If successful, go ahead and exit.
    return 0;
  } else {
    //!   - If there was a problem:
    //!     - Create a ServiceLogger object with the service name
    ServiceLogger logger( SERVICE_NAME );
    //!     - Log error message in the Event Log.
    logger.ReportWinError ( L"StartServiceCtrlDispatcher" );
    return -1;
  }
  
}

