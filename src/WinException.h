/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file WinException.h contains the definition for the WinException class.
 */

#ifndef __WINEXCEPTION_H__
#define __WINEXCEPTION_H__

#include "defines.h"

#include <tchar.h>

/*! \brief WinException is the base exception class for SimpleWinFingerd.
 */
class WinException {

 public:
  //! Maximum message size. Messages longer than this size are truncated.
  static const int MAX_MESSAGE_SIZE = 80;

  /*! \brief Default constructor.
   */
  WinException () throw();

  /*! \brief Constructor that takes a pointer to a message.
   */
  WinException ( const wchar_t* errorMesssage //!< [in] Error message.
		 ) throw();

  /*! \brief Constructor that takes an error code.
   */
  WinException ( DWORD errorCode //!< [in] Error code.
		 ) throw();

  /*! \brief Constructor that takes both a message and an error code.
   */
  WinException ( DWORD errorCode, //!< [in] Error code.
		 const wchar_t* errorMesssage //!< [in] Error message.
		 ) throw();

  /*! \brief Copy Constructor.
   */
  WinException ( const WinException& rhs //!< [in] Object to copy.
		 ) throw();

  /*! \brief Overloaded assignment operator.
   */
  WinException& operator= ( const WinException& rhs //! [in] Object to copy.
			    ) throw();
  
  /*! \brief Virtual destructor.
   */
  virtual ~WinException () throw();

  /*! \brief Retrieves a pointer to the message associated with this exception.
   */
  virtual const wchar_t* GetErrorMessage () throw();

  /*! \brief Retrieves the error code associated with this exception.
   */
  virtual WORD GetErrorCode () throw();

 private:
  /*! \brief Sets the message associated with this exception to the specified
   *  message.
   */
  void SetMessage ( const wchar_t* message //!< [in] Message to copy.
		    ) throw();
  
  //! Exception message, returned via GetErrorMessage(). Note that _msg is
  //! actually one TCHAR longer than MAX_MESSAGE_SIZE to ensure space for
  //! the terminating NULL.
  wchar_t _msg[MAX_MESSAGE_SIZE+1];

  //! Error code for this exception, returned via GetErrorCode().
  WORD _code;
};

#endif
