/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file WinException.cpp contains the function definitions for the 
 * WinException class. See WinException.h for the class definition.
 */

#include "defines.h"

#include <cwchar>
#include <windows.h>

#include "WinException.h"

WinException::WinException () throw() {
  //! Algorithm:
  //! <ol>

  //! <li> Initialize variables
  _msg[0] = (wchar_t)NULL;
  _code = 0;

  //! </ol>
}

WinException::WinException ( const wchar_t* errorMessage ) throw() {
  //! Algorithm:
  //! <ol>

  //! <li> Initialize variables
  _msg[0] = (wchar_t)NULL;
  _code = 0;

  //! <li> Copy error message.
  SetMessage ( errorMessage );

  //! </ol>
}

WinException::WinException ( DWORD errorCode ) throw() {
  //! Algorithm:
  //! <ol>

  //! <li> Initialize variables
  _msg[0] = (wchar_t)NULL;
  _code = 0;

  //! <li> Set error code
  _code = errorCode;

  //! </ol>
}

WinException::WinException ( DWORD errorCode,
			     const wchar_t* errorMessage ) throw() {
  //! Algorithm:
  //! <ol>

  //! <li> Initialize variables
  _msg[0] = (wchar_t)NULL;
  _code = 0;

  //! <li> Set error code
  _code = errorCode;

  //! <li> Set error message
  SetMessage ( errorMessage );

  //! </ol>
}

WinException::WinException ( const WinException& rhs ) throw() {
  //! Algorithm:
  //! <ol>

  //! <li> Initialize variables
  _msg[0] = (wchar_t)NULL;
  _code = 0;

  //! <li> Call the overloaded operator=.
  *this = rhs;
 
  //! </ol>
}

WinException& WinException::operator= ( const WinException& rhs ) throw() {

  //! Algorithm:
  //! <ol>

  //! <li> Check for self-assignment.


  //! <li> Copy code
  _code = rhs._code;
  
  //! <li> Copy message
  SetMessage ( &rhs._msg[0] );

  //! <li> Return *ths.
  return *this;
  //! </ol>
}
  
WinException::~WinException () throw() {
  //! Set error code to 0 and clear message.
  _code = 0;
  _msg[0] = (wchar_t)NULL;
}

const wchar_t* WinException::GetErrorMessage () throw() {
  return (const wchar_t*) &_msg[0];
}

WORD WinException::GetErrorCode () throw() {
  return _code;
}

void WinException::SetMessage ( const wchar_t* message ) throw() {

  try {
    //! # Copy message within a try block and catch (...)
    wcsncpy ( &_msg[0], message, MAX_MESSAGE_SIZE );
    
    //! # Set last wchar_t in _msg to NULL, just in case.
    _msg[MAX_MESSAGE_SIZE] = (wchar_t)NULL;
  } catch (...) {
    //! # If we catch an exception, set the first character of _msg to NULL
    //! and move on.
    _msg[0] = (wchar_t)NULL;
  }
}
