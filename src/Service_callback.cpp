/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file Service_callback.cpp contains the functional bodies for all Windows Service-related 
 *  callback functions. See Service_callback.h for more information.
 */

#include "defines.h"

#include <windows.h>
#include "Service.h"

/* 
 * Global Variable Definitions
 */

DWORD WINAPI ServiceControlHandler ( DWORD controlCode,
				     DWORD eventType,
				     void* eventData,
				     void* context ) {

  //! Process:

  // *** TO DO ***
  if ( context == NULL ) {
    // Handle error!
  }

  //! - Attempt to cast context into a Service*.
  Service* srv = (Service*)context;

  if ( srv == NULL ) {
    // Handle error!
  }

  //! - Pass on to Service->HandleControlCode().
  return srv->HandleControlCode ( controlCode, eventType, eventData );
}

void WINAPI ServiceMain( DWORD argc, 
			 LPTSTR* argv 
			 ) {

  //! Process:

  //! - Create the Service object.
  Service srv;

  //! - Pass control to the new Service object.
  srv.ServiceMain ( argc, argv );
}

