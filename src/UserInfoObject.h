/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file UserInfoObject.h contains the definition of the UserInfoObject
 *  class as well as related enumerations.
 */

#ifndef __USERINFOOBJECT_H__
#define __USERINFOOBJECT_H__

#include "defines.h"

#include <string>
#include <ctime>
using namespace std;

typedef enum _USER_INFO_CONNECTION_STATE {
  UserNotLoggedIn=1,
  UserDisconnected,
  UserConnectedConsole,
  UserConnectedRemote
} USER_INFO_CONNECTION_STATE;

/*! \brief UserInfoObject is to manage the user account info used by
 *  SimpleWinFingerd.
 *
 */
class UserInfoObject {
 public:
  
  /*! \brief Default constructor.
   */
  UserInfoObject ( );
  
  /*! \brief Copy constructor.
   */
  UserInfoObject ( const UserInfoObject& rhs );
  
  /*! \brief Destructor.
   */
  ~UserInfoObject ( );

  /*! \brief Retrieves the account name for this object.
   *
   *  \return Account name.
   */
  wstring GetName ( );

  /*! \brief Sets the account name for this object.
   */
  void SetName ( wstring name //!< [in] Account name to associate with this 
		 //!< object.
		 );

  /*! \brief Gets the user's connection state.
   *
   *  \return The user's connection state.
   */
  USER_INFO_CONNECTION_STATE GetConnectionState ( );

  /*! \brief Sets the user's connection state.
   */
  void SetConnectionState ( USER_INFO_CONNECTION_STATE newState //!< [in] The
			    //!< new connection state of this user.
			    );

  /*! \brief Gets whether this user has locked the screen or not.
   *
   *  \return True if the screen is locked, false otherwise.
   */
  bool IsScreenLocked ( );

  /*! \brief Sets whether this user has locked the screen.
   */
  void SetScreenLocked ( bool locked //!< [in] True if the screen is locked.
			 );

  /*! \brief Sets the connect time for this record.
   */
  void SetConnectTime ( const time_t& connectTime //!< [in] User connect time.
			);
  /*! \brief Gets the connect time for this record.
   *
   *  \return data structure containing the user connection time.
   */
  time_t GetConnectTime ( );

  /*! \brief Sets the login time for this record.
   */
  void SetLoginTime ( const time_t& loginTime //!< [in] User login time.
			);
  /*! \brief Gets the login time for this record.
   *
   *  \return data structure containing the user login time.
   */
  time_t GetLoginTime ( );

  /*! \brief Gets whether this record is stale or not.
   *
   *  \return True if record is stale.
   */
  bool GetRecordStale ( );
  
  /*! \brief Sets whether this record is stale or not.
   */
  void SetRecordStale ( bool isStale //!< [in] True if record is stale.
			);

  /*! \brief Overloaded Operator=.
   */
  UserInfoObject& operator= ( const UserInfoObject& rhs //!< [in] Right-hand 
			      //!< side of statement.
			      );

  /*! \brief Overloaded Operator==.
   */
  bool operator== ( const UserInfoObject& rhs //!< [in] Right-hand 
		    //!< side of statement.
		    );

  /*! \brief Overloaded Operator!=, which is just inverse of operator==
   */
  bool operator!= ( const UserInfoObject& rhs //!< [in] Right-hand 
		    //!< side of statement.
		    );
  
 private:

  //! Initializes internal data structures.
  void InitData ( );
  
  //! User account name
  wstring _name;

  //! State of user connection
  USER_INFO_CONNECTION_STATE _state;

  //! Whether the user has locked the screen or not.
  bool _screenLocked;

  //! Flag to indicate whether this record is stale or not. Used
  //! primarily by the manager object to figure out which records to delete.
  bool _recordStale;

  //! User connection time.
  time_t _connectTime;

  //! Initial user login time
  time_t _loginTime;
};

#endif
