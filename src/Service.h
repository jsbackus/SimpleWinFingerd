/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file Service.h contains the definition of the Service class.
 */

#ifndef __SERVICE_H__
#define __SERVICE_H__

#include "defines.h"

#include <windows.h>

#include "ServiceLogger.h"
#include "ConfigManager.h"
#include "UserInfoManager.h"

/*! \brief Service is a class that encapsulates the various functions of a Windows Service.
 *
 *  All required C-like callback functions required by the Service Control Manager (SCM) can be found in
 *  Service_callback.h and Service_callback.cpp.
 */
class Service {
 public:
  
  /*! \brief Default constructor.
   */
  Service ();
  
  /*! \brief Destructor.
   */
  ~Service ();
  
  /*! \brief This is the main entry point for the service.
   *
   *  ServiceMain() is responsible for:
   *  - Registering the service.
   *  - Initializing:
   *    - The ServiceLogger
   *    - The Configuration Manager
   *    - The TCP Server
   *    - The User Monitor
   *  - The main processing loop.
   *  - Cleaning up when the service is stopped.
   */
  void ServiceMain ( DWORD argc, //!< [in] Number of arguments passed by Service Controller.
		     LPTSTR* argv //!< [in] Pointer to the null-terminated arguments passed by
		     //!< Service Controller.
		     );

  /*! \brief Function called by the installed ServiceControlHandlerEx to handle control codes.
   * 
   *  For more info, see:
   *  - [ControlServiceEx](http://msdn.microsoft.com/en-us/library/ms682110%28v=vs.85%29.aspx)
   *  - [HandlerEx](http://msdn.microsoft.com/en-us/library/ms683241%28v=vs.85%29.aspx)
   *
   * \return Determined by the definition of HandlerEx().
   */
  DWORD HandleControlCode ( DWORD controlCode, //!< [in] Control code from the SCM.
			    DWORD eventType, //!< [in] Type of event that has occurred.
			    void* eventData //!< [in] Additional optional device information.
			    );

 private:
  /*! \brief Reports the status of the service to the SCM.
   */
  void ReportStatus ( DWORD state //!< [in] Updated state of the service.
		      );

  /*! \brief Handles user status changes as indicted by 
   *  SERVICE_CONTROL_SESSIONCHANGE.
   *
   *  For more information, see:
   *  [WM_WTSESSION_CHANGE](http://msdn.microsoft.com/en-us/library/aa383828%28v=VS.85%29.aspx)
   *  [WTSQuerySessionInformation](http://msdn.microsoft.com/en-us/library/windows/desktop/aa383838%28v=vs.85%29.aspx)
   *
   */
  void UpdateUserStatus ( DWORD eventType, //!< [in] User state change type.
			  DWORD sessionID //!< [in] The session ID that the 
			  //!< change applies to.
			  );

  //! Handle to the service once it has been registerd.
  SERVICE_STATUS_HANDLE _serviceStatusHandle;

  //! Handle of event used to signal that the service is being stopped.
  HANDLE _serviceStopEvent;

  //! The current state of the service.
  DWORD _currentState;
  
  //! Flag indicating that the system is being shut down.
  bool _bSystemShutdown;

  //! ServiceLogger Object.
  ServiceLogger _logger;

  //! ConfigManager Object.
  ConfigManager _configMgr;

  //! UserInfoManager Object.
  UserInfoManager _userMgr;
};

#endif
