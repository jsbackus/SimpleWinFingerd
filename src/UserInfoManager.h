/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file UserInfoManager.h contains the definition of the UserInfoManager 
 *  class.
 */

#ifndef __USERINFOMANAGER_H__
#define __USERINFOMANAGER_H__

#include "defines.h"

#include <windows.h>

#include <string>
#include <map>
#include <vector>

using namespace std;

#include "ServiceLogger.h"
#include "ConfigManager.h"
#include "UserInfoObject.h"

/*! \brief UserInfoManager is a class to manage user login state info.
 *
 */
class UserInfoManager {
 public:
  
  /*! \brief Default constructor.
   */
  UserInfoManager ( );

  /*! \brief Destructor.
   */
  ~UserInfoManager ();

  /*! \brief Initializes this UserInfoManager object.
   */
  void Initialize ( ServiceLogger logger, //!< [in] ServiceLogger object
		    ConfigManager configManager //!< [in] ConfigManager object
		    );
  
  /*! \brief Handles user status changes as indicted by 
   *  SERVICE_CONTROL_SESSIONCHANGE.
   *
   *  This function should be called by HandleControlCode() of Service.
   *
   *  For more information, see:
   *  [WM_WTSESSION_CHANGE](http://msdn.microsoft.com/en-us/library/aa383828%28v=VS.85%29.aspx)
   *  [WTSQuerySessionInformation](http://msdn.microsoft.com/en-us/library/windows/desktop/aa383838%28v=vs.85%29.aspx)
   *
   */
  void UpdateUserStatus ( DWORD eventType, //!< [in] User state change type.
			  DWORD sessionID //!< [in] The session ID provided by
			  //!< SERVICE_CONTROL_SESSIONCHANGE.
			  );

  /*! \brief Retrieve the UserInfoObject for the specified user.
   *
   *  If the specified user is not logged in, then a default object is returned.
   *  If the specified username is invalid, a default object is returned
   *  indicating that the specified user is not logged in.
   *
   *  \return UserInfoObject for the specied user.
   */
  UserInfoObject GetUserInfo ( wstring userName //!< [in] Name of user to query.
			       );

  /*! \brief Retrieve the UserInfoObjects for all logged in users.
   *
   *  \return The list of UserInfoObjects for all logged in users.
   */
  vector<UserInfoObject> GetLoggedInUsers ( );

 private:

  /*! \brief Finds the UserInfoObject for the specified user.
   *
   *  If the user is not in the database, the user is added and the Log In
   *  time is set.
   *
   *  Returns NULL if there is an error.
   *
   *  \return UserInfoObject for the specified user.
   */
  UserInfoObject* GetUserRecord ( wstring userName //!< [in] Name of user.
				  );

  //! ServiceLogger Object.
  ServiceLogger _logger;

  //! ConfigManager Object.
  ConfigManager _configMgr;

  //! List of currently logged in users
  map<wstring, UserInfoObject> _users;

};

#endif
