/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file
 * Unit tests for UserinfoObject.
 */

#include "UserInfoObject.h"

#include "test_helper.h"

int TestGetSetName() {

  try{
    UserInfoObject obj;
    
    ASSERT( obj.GetName() == L"" );

    obj.SetName( L"Rufus" );
    ASSERT( obj.GetName() == L"Rufus" );
    
  } catch( exception& e ) {
    cout << "Caught exception: " << e.what() << endl;
    return 1;
  }

  return 0;
}

int TestGetSetConnectionState() {

  try{
    UserInfoObject obj;
    
    ASSERT( obj.GetConnectionState() == UserDisconnected );

    obj.SetConnectionState( UserNotLoggedIn );
    ASSERT( obj.GetConnectionState() == UserNotLoggedIn );
    
    obj.SetConnectionState( UserDisconnected );
    ASSERT( obj.GetConnectionState() == UserDisconnected );
    
    obj.SetConnectionState( UserConnectedConsole );
    ASSERT( obj.GetConnectionState() == UserConnectedConsole );
    
    obj.SetConnectionState( UserConnectedRemote );
    ASSERT( obj.GetConnectionState() == UserConnectedRemote );
    
  } catch( exception& e ) {
    cout << "Caught exception: " << e.what() << endl;
    return 1;
  }

  return 0;
}

int TestGetSetScreenLocked() {

  try{
    UserInfoObject obj;

    ASSERT( !obj.IsScreenLocked() );

    obj.SetScreenLocked( true );
    ASSERT( obj.IsScreenLocked() );

    obj.SetScreenLocked( false );
    ASSERT( !obj.IsScreenLocked() );
    
  } catch( exception& e ) {
    cout << "Caught exception: " << e.what() << endl;
    return 1;
  }

  return 0;
}

int TestGetSetConnectTime() {

  try{
    time_t defaultTime;
    time_t now = time(NULL);
    UserInfoObject obj;

    ASSERT( difftime(defaultTime, obj.GetConnectTime()) == 0 );

    obj.SetConnectTime( now );
    ASSERT( difftime(now, obj.GetConnectTime()) == 0 );

  } catch( exception& e ) {
    cout << "Caught exception: " << e.what() << endl;
    return 1;
  }

  return 0;
}

int TestGetSetLoginTime() {

  try{
    time_t defaultTime;
    time_t now = time(NULL);
    UserInfoObject obj;

    ASSERT( difftime(defaultTime, obj.GetConnectTime()) == 0 );

    obj.SetConnectTime( now );
    ASSERT( difftime(now, obj.GetConnectTime()) == 0 );

  } catch( exception& e ) {
    cout << "Caught exception: " << e.what() << endl;
    return 1;
  }

  return 0;
}

int TestGetSetRecordStale() {

  try{
    UserInfoObject obj;

    ASSERT( obj.GetRecordStale() );

    obj.SetRecordStale( false );
    ASSERT( !obj.GetRecordStale() );

    obj.SetRecordStale( true );
    ASSERT( obj.GetRecordStale() );

  } catch( exception& e ) {
    cout << "Caught exception: " << e.what() << endl;
    return 1;
  }

  return 0;
}

int TestOpEqualsEquals() {

  try{
    struct tm tmpTime;
    time_t timeConnA;
    time_t timeConnB;
    time_t timeLoginA;
    time_t timeLoginB;

    UserInfoObject obj1;
    UserInfoObject obj2;

    // Populate tmpTime with current time, and then tweak some fields
    timeConnA = time( NULL );
    tmpTime = *localtime( &timeConnA );
    
    tmpTime.tm_hour = 4;
    tmpTime.tm_min = 42;
    timeConnA = mktime( &tmpTime );

    tmpTime.tm_hour = 7;
    tmpTime.tm_min = 17;
    timeConnB = mktime( &tmpTime );
    ASSERT( 0 < difftime( timeConnB, timeConnA ) );

    tmpTime.tm_hour = 5;
    tmpTime.tm_min = 55;
    timeLoginA = mktime( &tmpTime );

    tmpTime.tm_hour = 2;
    tmpTime.tm_min = 37;
    timeLoginB = mktime( &tmpTime );
    ASSERT( 0 < difftime( timeLoginA, timeLoginB ) );

    obj1.SetName( L"fred" );
    obj2.SetName( L"fred" );

    obj1.SetConnectionState( UserConnectedConsole );
    obj2.SetConnectionState( UserConnectedConsole );

    obj1.SetScreenLocked( true );
    obj2.SetScreenLocked( true );

    obj1.SetRecordStale( true );
    obj2.SetRecordStale( true );

    obj1.SetConnectTime( timeConnA );
    obj2.SetConnectTime( timeConnA );

    obj1.SetLoginTime( timeLoginA );
    obj2.SetLoginTime( timeLoginA );
    
    ASSERT( obj1 == obj2 );

    // Check name
    obj2.SetName( L"george" );
    ASSERT( obj1 != obj2 );
    obj2.SetName( L"fred" );
    ASSERT( obj1 == obj2 );

    // Check Connection State
    obj2.SetConnectionState( UserConnectedRemote );
    ASSERT( obj1 != obj2 );
    obj2.SetConnectionState( UserConnectedConsole );
    ASSERT( obj1 == obj2 );

    // Check Screen Locked
    obj2.SetScreenLocked( false );
    ASSERT( obj1 != obj2 );
    obj2.SetScreenLocked( true );
    ASSERT( obj1 == obj2 );

    // Check Record Stale
    obj2.SetRecordStale( false );
    ASSERT( obj1 != obj2 );
    obj2.SetRecordStale( true );
    ASSERT( obj1 == obj2 );

    // Check Connect Time
    obj2.SetConnectTime( timeConnB );
    ASSERT( obj1 != obj2 );
    obj2.SetConnectTime( timeConnA );
    ASSERT( obj1 == obj2 );

    // Check Login Time
    obj2.SetLoginTime( timeLoginB );
    ASSERT( obj1 != obj2 );
    obj2.SetLoginTime( timeLoginA );
    ASSERT( obj1 == obj2 );    

  } catch( exception& e ) {
    cout << "Caught exception: " << e.what() << endl;
    return 1;
  }

  return 0;
}

int TestOpEquals() {

  try{
    struct tm tmpTime;
    time_t timeConnA;
    time_t timeConnB;
    time_t timeLoginA;
    time_t timeLoginB;

    UserInfoObject obj1;
    UserInfoObject obj2;

    // Populate tmpTime with current time, and then tweak some fields
    timeConnA = time( NULL );
    tmpTime = *localtime( &timeConnA );
    
    tmpTime.tm_hour = 4;
    tmpTime.tm_min = 42;
    timeConnA = mktime( &tmpTime );

    tmpTime.tm_hour = 7;
    tmpTime.tm_min = 17;
    timeConnB = mktime( &tmpTime );
    ASSERT( 0 < difftime( timeConnB, timeConnA ) );

    tmpTime.tm_hour = 5;
    tmpTime.tm_min = 55;
    timeLoginA = mktime( &tmpTime );

    tmpTime.tm_hour = 2;
    tmpTime.tm_min = 37;
    timeLoginB = mktime( &tmpTime );
    ASSERT( 0 < difftime( timeLoginA, timeLoginB ) );

    obj1.SetName( L"fred" );
    obj2.SetName( L"george" );

    obj1.SetConnectionState( UserConnectedConsole );
    obj2.SetConnectionState( UserConnectedRemote );

    obj1.SetScreenLocked( true );
    obj2.SetScreenLocked( false );

    obj1.SetRecordStale( true );
    obj2.SetRecordStale( false );

    obj1.SetConnectTime( timeConnA );
    obj2.SetConnectTime( timeConnB );

    obj1.SetLoginTime( timeLoginA );
    obj2.SetLoginTime( timeLoginB );
    
    ASSERT( obj1 != obj2 );

    obj2 = obj1;
    ASSERT( obj1 == obj2 );
    
    obj2.SetName( L"henry" );
    ASSERT( obj1 != obj2 );

  } catch( exception& e ) {
    cout << "Caught exception: " << e.what() << endl;
    return 1;
  }

  return 0;
}

int TestCopyConstructor() {

  try{
    struct tm tmpTime;
    time_t timeConnA;
    time_t timeLoginA;

    UserInfoObject obj1;

    // Populate tmpTime with current time, and then tweak some fields
    timeConnA = time( NULL );
    tmpTime = *localtime( &timeConnA );
    
    tmpTime.tm_hour = 4;
    tmpTime.tm_min = 42;
    timeConnA = mktime( &tmpTime );

    tmpTime.tm_hour = 5;
    tmpTime.tm_min = 55;
    timeLoginA = mktime( &tmpTime );

    obj1.SetName( L"fred" );

    obj1.SetConnectionState( UserConnectedConsole );

    obj1.SetScreenLocked( true );

    obj1.SetRecordStale( true );

    obj1.SetConnectTime( timeConnA );

    obj1.SetLoginTime( timeLoginA );

    UserInfoObject obj2( obj1 );
    
    ASSERT( obj1 == obj2 );
    
    obj2.SetName( L"henry" );
    ASSERT( obj1 != obj2 );

  } catch( exception& e ) {
    cout << "Caught exception: " << e.what() << endl;
    return 1;
  }

  return 0;
}

void UserInfoObjectTests() {
  RUN_TEST( TestGetSetName() );
  RUN_TEST( TestGetSetConnectionState() );
  RUN_TEST( TestGetSetScreenLocked() );
  RUN_TEST( TestGetSetConnectTime() );
  RUN_TEST( TestGetSetLoginTime() );
  RUN_TEST( TestGetSetRecordStale() );
  RUN_TEST( TestOpEqualsEquals() );
  RUN_TEST( TestOpEquals() );
  RUN_TEST( TestCopyConstructor() );
}
