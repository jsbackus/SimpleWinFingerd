/*
 * Copyright (c) 2012-2017, Jeff Backus
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright notice, this 
 *     list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation 
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*! \file
 * Contains the implementations for various unit test helper functions
 */

#include<iostream>

using namespace std;

static int _numPass = 0;
static int _numExec = 0;

void runTest( int result ) {
  _numExec++;
  if( result == 0 ) {
    cout << " Passed\n";
    _numPass++;
    /*    
  } else {
    cout << " FAILED!\n";
    */
  }
}

void runFixture( const char* fixtureName, void fixture() ) {
  cout << endl << "Running Fixture " << fixtureName << ":" << endl;
  int lclPass = _numPass;
  int lclExec = _numExec;
  (*fixture)();
  lclPass = _numPass - lclPass;
  lclExec = _numExec - lclExec;
  cout << "  ---> " << _numPass << " of " << _numExec << " passed" << endl;
}

void printSummary() {
  cout << endl << endl;
  cout << "------------------------------------------------" << endl;
  cout << "Testing complete: " << _numPass << " of "
       << _numExec << " passed!" << endl << endl;
}
